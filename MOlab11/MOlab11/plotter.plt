 #Zaleznosc max wartosci bezwzglednej bledu w funkcji czasu t dla Laasonen + Thomas
set xrange [0 : 2.1]
set yrange [ 1e-6 : 0.005]
set terminal png size 1366,768
set output "Wykres_max_bledu_od_t_thomas.png"
set title 'Wykres bledu od t dla Laasonen + Thomas'
set ylabel 'blad'
set xlabel 't'
set grid 
plot \
 "max_blad_thomas.txt" using 1:2 with  points pt 1 lc 8 title "Klasyczna metoda bezposrednia",\
 
  #Zaleznosc max wartosci bezwzglednej bledu w funkcji czasu t dla laasonen + Jacobi
set xrange [0 : 2.1]
set yrange [ 1e-6 : 0.005]
set terminal png size 1366,768
set output "Wykres_max_bledu_od_t_jacobi.png"
set title 'Wykres bledu od t dla Laasonen + Jacobi'
set ylabel 'blad'
set xlabel 't'
set grid 
plot \
 "max_blad_jacobi.txt" using 1:2 with  points pt 1 lc 8 title "Klasyczna metoda bezposrednia",\

  #Zaleznosc max wartosci bezwzglednej bledu dla t_max w funkcji kroku Jacobi
set xrange [-1.75 : 0]
set yrange [ -4.5 : -1]
set terminal png size 1366,768
set output "max_blad_w_tmax_jacobi.png"
set title 'Wykres bledu w zaleznosci od kroku h dla algorytmu Jacobiego'
set ylabel 'log10(blad)'
set xlabel 'log10(h)'
set grid 
plot \
 "bledy_od_kroku_jacobi.txt" using 2:3 with points pt 13 lc 1 title "Laasonen + Jacobi",\
 "bledy_od_kroku_jacobi.txt" using 2:3 with lines title "Laasonen + Jacobi",\


  #Zaleznosc max wartosci bezwzglednej bledu dla t_max w funkcji kroku Thomas
set xrange [-1.75 : 0]
set yrange [ -4.5 : -1]
set terminal png size 1366,768
set output "max_blad_w_tmax_thomas.png"
set title 'Wykres bledu w zaleznosci od kroku h dla algorytmu Thomasa'
set ylabel 'log10(blad)'
set xlabel 'log10(h)'
set grid 
plot \
 "bledy_od_kroku_thomas.txt" using 2:3 with points pt 3 lc 3 title "Laasonen + Thomas",\
 "bledy_od_kroku_thomas.txt" using 2:3 with lines title "Laasonen + Thomas",\

  #Wartosci zadanej funkcji dla t=0.5 i algorytmu thomasa
set xrange [-10 : 10]
set yrange [-10 : 10]
set terminal png size 1366,768
set output "Rozwiazanie_Thomas_t_0.5.png"
set title 'Rozwiazanie Laasonen + Thomas t = 0.5'
set ylabel 'U(x,0.5)'
set xlabel 'x'
set grid 
plot \
 "wartosci_analityczne.txt" using 1:2 with lines title "Rozwiazanie Analityczne",\
 "wartosci_thomas.txt" using 1:2 with points pt 1 lc 8 title "Rozwiazanie Metoda Laasonen i algorytmem Thomasa",\

  #Wartosci zadanej funkcji dla t=1.0 i algorytmu thomasa
set xrange [0 : 9]
set yrange [0 : 1]
set terminal png size 1366,768
set output "Rozwiazanie_Thomas_t_1.0.png"
set title 'Rozwiazanie Laasonen + Thomas t = 1.0'
set ylabel 'U(x,1.0)'
set xlabel 'x'
set grid 
plot \
 "wartosci_analityczne.txt" using 1:3 with lines title "Rozwiazanie Analityczne",\
 "wartosci_thomas.txt" using 1:3 with points pt 1 lc 8 title "Rozwiazanie Metoda Laasonen i algorytmem Thomasa",\

  #Wartosci zadanej funkcji dla t=1.5 i algorytmu thomasa
set xrange [0 : 9]
set yrange [0 : 1]
set terminal png size 1366,768
set output "Rozwiazanie_Thomas_t_1.5.png"
set title 'Rozwiazanie Laasonen + Thomas t = 1.5'
set ylabel 'U(x,1.5)'
set xlabel 'x'
set grid 
plot \
 "wartosci_analityczne.txt" using 1:4 with lines title "Rozwiazanie Analityczne",\
 "wartosci_thomas.txt" using 1:4 with points pt 1 lc 8 title "Rozwiazanie Metoda Laasonen i algorytmem Thomasa",\

  #Wartosci zadanej funkcji dla t=0.5 i algorytmu jacobiego
set xrange [0 : 9]
set yrange [0 : 1]
set terminal png size 1366,768
set output "Rozwiazanie_Jacobi_t_0.5.png"
set title 'Rozwiazanie Laasonen + Jacobi t = 0.5'
set ylabel 'U(x,0.5)'
set xlabel 'x'
set grid 
plot \
 "wartosci_analityczne.txt" using 1:2 with lines title "Rozwiazanie Analityczne",\
 "wartosci_jacobi.txt" using 1:2 with points pt 1 lc 8 title "Rozwiazanie Metoda Laasonen i algorytmem Jacobiego",\

  #Wartosci zadanej funkcji dla t=1.0 i algorytmu jacobiego
set xrange [0 : 9]
set yrange [0 : 1]
set terminal png size 1366,768
set output "Rozwiazanie_Jacobi_t_1.0.png"
set title 'Rozwiazanie Laasonen + Jacobi t = 1.0'
set ylabel 'U(x,1.0)'
set xlabel 'x'
set grid 
plot \
 "wartosci_analityczne.txt" using 1:3 with lines title "Rozwiazanie Analityczne",\
 "wartosci_jacobi.txt" using 1:3 with points pt 1 lc 8 title "Rozwiazanie Metoda Laasonen i algorytmem Jacobiego",\

  #Wartosci zadanej funkcji dla t=1.5 i algorytmu jacobiego
set xrange [0 : 9]
set yrange [0 : 1]
set terminal png size 1366,768
set output "Rozwiazanie_Jacobi_t_1.5.png"
set title 'Rozwiazanie Laasonen + Jacobi t = 1.5'
set ylabel 'U(x,1.5)'
set xlabel 'x'
set grid 
plot \
 "wartosci_analityczne.txt" using 1:4 with lines title "Rozwiazanie Analityczne",\
 "wartosci_jacobi.txt" using 1:4 with points pt 1 lc 8 title "Rozwiazanie Metoda Laasonen i algorytmem Jacobiego",\

  #Wartosci zadanej funkcji dla t=1.0 i algorytmu jacobiego oraz Thomasa
set xrange [0 : 9]
set yrange [0 : 1]
set terminal png size 1366,768
set output "Rozwiazanie_zbiorcze_t_1.0.png"
set title 'Rozwiazanie zbiorcze dla t = 1.0'
set ylabel 'U(x,1.0)'
set xlabel 'x'
set grid 
plot \
 "wartosci_analityczne.txt" using 1:3 with lines title "Rozwiazanie Analityczne",\
 "wartosci_jacobi.txt" using 1:3 with points pt 5 lc 1 title "Rozwiazanie Metoda Laasonen i algorytmem Jacobiego",\
 "wartosci_thomas.txt" using 1:3 with points pt 3 lc 4 title "Rozwiazanie Metoda Laasonen i algorytmem Thomasa",\
