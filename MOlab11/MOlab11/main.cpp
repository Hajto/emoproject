﻿#include <windows.h>
#include "calerf.h"
#define _USE_MATH_DEFINES // for C++  
#include <cmath>  
#include <iostream>
#include <stdlib.h>
#include <fstream>
using namespace std;



/** Parametry pocz¹tkowe */
const double D=1.0;						//wspó³czynnik transportu ciep³a
const double t_max=2.0;					//maksymalna czas
const double t_min=0.0;					//minimalny czas
const double tau = double(0.1);
/*************************/
double a;								//górna granica dla wspó³rzêdnej przestrzennej "x"
double x_min;
double h;								//krok wspó³rzêdnej przestrzennej "x"
double dt;								//krok zmiennej czasu
double lambda;							//d¹zymy do osi¹gniêcia LAMBDA = 1 dla metod poœrednich
int M;									//liczba podzialow przedzialu przestrzennego (zmienna x)				
int N;									//liczba podzialow przedzialu czasowego (zmienna t)
double **anal;
double ** laasonen;
double *pozycja;
double *czas;

double TOL = 0.0000000001; //tolerancja b³êdu dla metody Jacobiego

enum metoda{ Thomas, LU ,None};  //Pomocnicza zmienna wyliczeniowa, do wyboru metody w Laasonen
enum typpliku{csv,txt};

double warunek_początkowy(double t, double x)
{
	return (
		1 / (
			2*sqrt(M_PI * D * (tau + t))
			)
		)*exp(
			-1 * (
				x*x
				) / (
					4*D*(tau+t)
				)
		);
}

void warunki(double** macierz)
{
	for (int j = 0; j<M; j++)
		macierz[0][j] = warunek_początkowy(0,j);			//warunek pocz¹tkowy U(x,0) = 0
	for (int i = 0; i<N; i++)
		macierz[i][M - 1] = 0;		//warunek brzegowy U(M,t) = 0
	for (int i = 0; i<N; i++)
		macierz[i][0] = 0;			//warunek brzegowy U(0,t) = 0
}

void zapisz_do_pliku(char *nazwa_pliku, double** macierz,typpliku typ)
{
	fstream plik;
	plik.open(nazwa_pliku, ios::out);
		switch (typ){
		case csv:
		{
			plik << ";";
			for (int i = 0; i<M; i++)
				plik << pozycja[i] << ";";
			plik << endl;
			for (int i = 0; i<N; i++)
			{
				plik << czas[i] << ";";
				for (int j = 0; j<M; j++)
				{
					plik << macierz[i][j] << ";";
				}
				plik << "\n";
			}
			break;
		}
		case txt:
		{
			for (int i = 0; i<N; i++)
			{
				for (int j = 0; j<M; j++)
				{
					plik << macierz[i][j] << " ";
				}
				plik << "\n";
			}
			break;
		}
		}
	plik.close();
}

void ustaw_zmienne(int m)
{
	a = 6 * sqrt(D*(t_max+tau));
	x_min = -a;
	M = m;
	h = (1*a) / (double)(m - 1);
	N = (int)((t_max / (h*h)) + 1);
	dt = t_max / (double)(N - 1);
	lambda = D * dt / (h * h);
	anal = new double*[N];
	for (int i = 0; i < N; i++)
		anal[i] = new double[M];

	laasonen = new double*[N];
	for (int i = 0; i < N; i++)
		laasonen[i] = new double[M];

	//wyliczona wartoœæ X w danym kroku 
	pozycja = new double[M];
	double x = x_min;
	for (int i = 0; i < M; i++)
	{
		pozycja[i] = x;
		x += h;
		cout << pozycja[i] << endl;
	}
		

	//wyliczona wartoœæ czasu T w danym kroku 
	czas = new double[N];
	for (int i = 0; i < N; i++)
		czas[i] = i*dt;
}

void rozwiazanie_analityczne()
{
	warunki(anal);

	double t = dt, x = -a;
	for (int i = 1; i<N; i++)
	{
		for (int j = 1; j<M - 1; j++)
		{
			anal[i][j] = ERFCL(x / (2 * sqrt(D*t))); // rozwi¹zanie analityczne u¿ywaj¹æ za³¹czonego pakietu calerf
			x = x + h;
		}
		x = -a;
		t = t + dt;
	}
	zapisz_do_pliku("rozw_analityczne.csv", anal,csv);
	zapisz_do_pliku("rozw_analityczne.txt", anal, txt);


}

void thomas(double **A, double *b, double *wyn)
{
	double *beta, *gamma;
	beta = new double[M]; gamma = new double[M];

	//obliczanie gammy i bety
	beta[0] = -(A[0][1] / A[0][0]);
	gamma[0] = (b[0] / A[0][0]);

	for (int i = 1; i<M; i++)//obliczanie wspó³czynników algorytmu thomasa
	{
		if (i <= M - 2)
			beta[i] = -((A[i][2 + i - 1]) / ((A[i][i - 1] * beta[i - 1]) + A[i][1 + i - 1]));

		gamma[i] = (b[i] - (A[i][i - 1] * gamma[i - 1])) / (A[i][i - 1] * beta[i - 1] + A[i][1 + i - 1]);
	}
	beta[M - 1] = 0;

	//obliczanie rozwi¹zania przy u¿yciu wspó³czynników
	wyn[M - 1] = gamma[M - 1];
	for (int i = M - 2; i >= 0; i--)
		wyn[i] = beta[i] * wyn[i + 1] + gamma[i];

	//zwolnienie zarezerwowanej pamiêci
	delete beta, gamma;
}

//norma max z wektora
double norma_max(double *p) 
{
	double MAX = 0.0;
	if (fabs(p[0])>fabs(p[1])) MAX = fabs(p[0]);
	else MAX = fabs(p[1]);
	for (int iter = 0; iter < M; iter++){
		if (fabs(p[iter]) > MAX) MAX = fabs(p[iter]);
	}
	return MAX;
}

//estymator b³êdu, przyjmuje dwa wektory jako argumenty
double estymator_bledu(double *xnew, double *xold)
{
	double MAX = 0.0;
	double *p = new double[M];
	for (int iter = 0; iter < M; iter++){
		p[iter] = xnew[iter] - xold[iter];
	}
	if (fabs(p[0])>fabs(p[1])) MAX = fabs(p[0]);
	else MAX = fabs(p[1]);
	for (int iter = 0; iter < M; iter++){
		if (fabs(p[iter]) > MAX) MAX = fabs(p[iter]);
	}
	return MAX;
}

//residuum, zwraca wektor z residualny
double* residuum(double **a, double *b, double *x) 
{
	double suma = 0;
	double *temp = new double[M];
	int i, j;
	for (i = 0; i<M; i++)
	{
		//obliczenie sumy elementów macierzy pomno¿onych przez wektor przybli¿eñ x
		for (j = 0; j<M; j++) suma = suma + a[i][j] * x[j];
		temp[i] = suma - b[i];
		suma = 0;
	}
	return temp;
}

void jacobi(double **A, double *b, double *x)
{
	double *xnew = new double[M];
	double suma = 0;
	for (int iter = 0; iter < 100; iter++)
	{
		for (int i = 0; i < M; i++)
		{
			//obliczenie kolejnych przybli¿eñ wektora x
			suma = 0.0;
			for (int j = 0; j < M; j++)
				if (j != i)
					suma += A[i][j] * x[j];
			xnew[i] = (1.0 / A[i][i]) * (b[i] - suma);
		}
		for (int i = 0; i < M; i++)
			x[i] = xnew[i];//wpisanie wyliczonych przybli¿eñ x do globalnego wektora x
		//warunki zakonczenia iteracji
		if (fabs((norma_max(residuum(A, b, x))))<TOL && fabs((estymator_bledu(xnew, x)))<TOL) break;  
	}
}

void rozw_laasonen(metoda metoda)
{
	double x = 0, t = 0;
	double *b = new double[M];
	for (int i = 0; i < M; i++) b[i] = 0;
	double *wyn = new double[M];
	for (int i = 0; i < M; i++) wyn[i] = 0;
	double **A = new double *[M];
	for (int i = 0; i < M; i++) A[i] = new double[M];
	//wyzerowanie macierzy
	for (int i = 0; i < M; i++)
		for (int j = 0; j < M; j++)
			A[i][j] = 0;

	warunki(laasonen);
	
	//wypelnianie macierzy 
	for (int k = 1; k < N; k++)
	{
		A[0][0] = 1;
		b[0] = 1; //wynika z lewego warunku brzegowego
		for (int i = 1; i < M - 1; i++)  //wpisanie wartoœci do macierzy trójdiagonalnej
		{
			A[i][i] = -(1 + (2 * lambda));
			A[i][i + 1] = lambda;
			A[i][i - 1] = lambda;
			b[i] = -laasonen[k - 1][i];
		}
		b[M - 1] = 0;	//wynika z prawego warunku brzegowego
		A[M - 1][M - 1] = 1;
		switch (metoda)	//sprawdzamy jak¹œ metod¹ chcemy wyliczyæ rozwi¹zanie uk³adu równañ
		{
		case Thomas:
			thomas(A, b, wyn);	//wyliczenie metod¹ Thomasa
			break;
		case LU:
			jacobi(A, b, wyn);	//wyliczenie metod¹ iteracyjn¹ Jacobiego
			break;
		}
		for (int i = 1; i < M - 1; i++)
			laasonen[k][i] = wyn[i];  // zapisanie do kolejnego wiersza wyliczonych wartoœci
	}
	//zapisanie rozwi¹zañ do pliku
	switch (metoda)
	{
	case Thomas:
		zapisz_do_pliku("rozw_laasonen_thomas.csv", laasonen,csv);
		break;
	case LU:
		zapisz_do_pliku("rozw_laasonen_jacobi.csv", laasonen,csv);
		break;
	}
	//usuwanie zarezerwowanej pamiêci
	for (int i = M - 1; i >= 0; i--) delete[]A[i];
	delete[]A;
	delete b, wyn;
}

void max_blad(int freq,metoda method)
{
	fstream bledy;
	//wybór metody
	if (method == Thomas) {
		bledy.open("max_blad_thomas.txt", ios::out);
	}
	else {
		bledy.open("max_blad_jacobi.txt", ios::out);
	}
	if (bledy.is_open())
	{
		double err;
		double max;
		for (int j = 0; j < N; j += freq)
		{
			max = 0;
			for (int i = 0; i < M; i++)
			{
				//max z b³êdów dla poszczególnych wierszy
				err = laasonen[j][i] - anal[j][i];
				err = fabs(err);
				if (err > max)
					max = err;
			}
			bledy << czas[j] << " " << max << endl;
		}
		bledy.close();
	}
}

void wykres_bledu_od_kroku(metoda method){
	int zakres[9] = { 10, 25, 50, 75, 100, 125, 150, 200, 300 };
	fstream bledy;
	if(method==Thomas) bledy.open("bledy_od_kroku_thomas.txt", ios::out);
	else bledy.open("bledy_od_kroku_jacobi.txt", ios::out);
	if (bledy.is_open())
	{
		for (int j = 0; j < 9; j++){
			double err;
			double max;
			//wykonujemy obliczenia od nowa z nowym zakresem
			ustaw_zmienne(zakres[j]);
			rozwiazanie_analityczne();
			rozw_laasonen(method);
			max = 0;
			for (int i = 0; i < M; i++)
			{
				err = laasonen[N - 1][i] - anal[N - 1][i];
				err = fabs(err);
				if (err > max)
					max = err;
			}
			bledy  << M <<" "<<log10(h)<<" "<< log10(max) << "\n";
		}
		bledy.close();
	}
}

void wykresy_wartosci(metoda method){
	fstream wykres;
	if (method == None){
		wykres.open("wartosci_analityczne.txt",ios::out);
	}
	if (method == Thomas){
		wykres.open("wartosci_thomas.txt",ios::out);
	}
	if (method == LU){
		wykres.open("wartosci_jacobi.txt",ios::out);
	}
	//wybrane wiersze w których wartoœæ t jest równa 0.5 1.0 oraz 1.5
	int pos[3] = { 622, 1243, 1864 };
	for (int col = 0; col < M; col++){
		wykres << pozycja[col] << " ";
		for (int row = 0; row < 3; row++){
			if (method == None){
				wykres << anal[pos[row]][col] << " ";
			}
			else{
				wykres << laasonen[pos[row]][col] << " ";
			}
		}
		wykres << endl;
	}
	wykres.close();

}

int main(){
	wykres_bledu_od_kroku(LU);
	printf("Projekt 11-5 - Metody obliczeniowe\nDyskretyzacja: \n  Metoda posrednia Laasonen\n");
	printf("Rozwiazanie algebraicznych ukladow rownan liniowych:\n  Metoda Iteracyjna Jacobiego\n  Algorytm Thomasa\n\n");
	printf("Parametry:\n D = %.2f \n t_max = %.2f \n Przedzial zmiennej przestrzennej x: [0;%.4f]\n", D, t_max, a);
	max_blad(10,LU);
	wykresy_wartosci(None);
	wykresy_wartosci(LU);
	wykres_bledu_od_kroku(Thomas);
	max_blad(10,Thomas);
	wykresy_wartosci(Thomas);
	system("PAUSE");
}